######################################################################################
##
##    Copyright (c) 2021 MR EVGENII BIRIALTCEV, SOLE TRADE <mail@rndnet.net>
##
##    This file is part of RnDnet.
##
##    RnDnet is free software: you can redistribute it and/or modify
##    it under the terms of the GNU General Public License as published by
##    the Free Software Foundation, either version 3 of the License, or
##    (at your option) any later version.
##
##    RnDnet is distributed in the hope that it will be useful,
##    but WITHOUT ANY WARRANTY; without even the implied warranty of
##    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##    GNU General Public License for more details.
##
##    You should have received a copy of the GNU General Public License
##    along with RnDnet.  If not, see <https://www.gnu.org/licenses/>.
#####################################################################################
import subprocess
import os, sys

pyver = sys.version_info

#----------------------------------------------------------------------------
# Get version string from git
#
# Author: Douglas Creager <dcreager@dcreager.net>
# http://dcreager.net/2010/02/10/setuptools-git-version-numbers/
#
# PEP 386 adaptation from
# https://gist.github.com/ilogue/2567778/f6661ea2c12c070851b2dfb4da8840a6641914bc
#----------------------------------------------------------------------------
def call_git_describe(abbrev=4):
    try:
        return subprocess.check_output(
                ['git', 'describe', '--abbrev={}'.format(abbrev)]
                ).decode().splitlines()[0]
    except:
        return None


def read_release_version():
    try:
        with open('{}/RELEASE-VERSION'.format(os.path.dirname(__file__)), 'r') as f:
            return f.readlines()[0].strip()
    except:
        return None


def write_release_version(version):
    with open('{}/RELEASE-VERSION'.format(os.path.dirname(__file__)), 'w') as f:
        f.write("{}\n".format(version))


def pep386adapt(version):
    # adapt git-describe version to be in line with PEP 386
    parts = version.split('-')
    if len(parts) > 1:
        parts[-2] = 'post'+parts[-2]
        version = '.'.join(parts[:-1])
    return version


def git_version(abbrev=4):
    # Read in the version that's currently in RELEASE-VERSION.
    release_version = read_release_version()

    # Try to get the current version using "git describe".
    version = call_git_describe(abbrev)

    # If that doesn't work, fall back on the value that's in
    # RELEASE-VERSION.
    if version is None:
        version = release_version
    else:
        #adapt to PEP 386 compatible versioning scheme
        version = pep386adapt(version)

    # If we still don't have anything, that's an error.
    if version is None:
        raise ValueError("Cannot find the version number!")

    # If the current version is different from what's in the
    # RELEASE-VERSION file, update the file to be current.
    if version != release_version:
        write_release_version(version)

    # Update the ev3dev/version.py
    with open('{}/bfj/version.py'.format(os.path.dirname(__file__)), 'w') as f:
        f.write("__version__ = '{}'".format(version))

    # Finally, return the current version.
    return version

