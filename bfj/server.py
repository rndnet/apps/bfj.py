######################################################################################
##
##    Copyright (c) 2021 MR EVGENII BIRIALTCEV, SOLE TRADE <mail@rndnet.net>
##
##    This file is part of RnDnet.
##
##    RnDnet is free software: you can redistribute it and/or modify
##    it under the terms of the GNU General Public License as published by
##    the Free Software Foundation, either version 3 of the License, or
##    (at your option) any later version.
##
##    RnDnet is distributed in the hope that it will be useful,
##    but WITHOUT ANY WARRANTY; without even the implied warranty of
##    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##    GNU General Public License for more details.
##
##    You should have received a copy of the GNU General Public License
##    along with RnDnet.  If not, see <https://www.gnu.org/licenses/>.
#####################################################################################
import os
import ssl
import urllib3
import requests
import traceback
import shutil
import functools

from pathlib                              import Path
from box                                  import Box
from requests.adapters                    import HTTPAdapter
from requests.packages.urllib3.util.retry import Retry
from threading                            import Lock

from .sha1 import *

urllib3.disable_warnings()
ssl._create_default_https_context = ssl._create_unverified_context

#---------------------------------------------------------------------------
def make_session():
    s = requests.Session()
    # https://www.peterbe.com/plog/best-practice-with-retries-with-requests
    adapter = HTTPAdapter(max_retries=Retry(
        total=3, read=3, connect=3,
        backoff_factor=0.3, status_forcelist=(500,502,504)))
    s.mount('http://', adapter)
    s.mount('https://', adapter)
    s.verify = False

    rndnet_token = os.environ.get('RNDNET_TOKEN')
    if rndnet_token:
        s.headers.update(Authorization=f'Token {rndnet_token}')

    return s

#---------------------------------------------------------------------------
def boxed(data):
    if isinstance(data, list):
        return [Box(row) if isinstance(row, dict) else row
                for row in data]

    if isinstance(data, dict):
        return Box(data)

    return data

#---------------------------------------------------------------------------
def boxed_request(f):
    def wrap(*args, **kwargs):
        try:
            r = f(*args, **kwargs)
            r.raise_for_status()
            return boxed(r.json())
        except:
            print(traceback.format_exc())
            raise

    return wrap

#---------------------------------------------------------------------------
def link_or_copy(src, dst):
    try:
        os.link(src, dst)
    except OSError:
        shutil.copyfile(src, dst)

#---------------------------------------------------------------------------
def ignore_errors(f):
    def wrap(*args, **kwargs):
        try:
            return f(*args, **kwargs)
        except:
            return
    return wrap

#---------------------------------------------------------------------------
class FileLRUCache:
    MAX_FILES = 128
    CLEANUP_FREQ = 100

    def __init__(self, path, max_files=MAX_FILES):
        self.path      = Path(path)
        self.max_files = max_files
        self.cleanup_counter = -1
        self.cleanup_lock = Lock()

        self.path.mkdir(parents=True, exist_ok=True)

    @ignore_errors
    def get(self, name):
        path = self.path / name
        if not path.is_file():
            return

        path.touch()
        return path

    @ignore_errors
    def put_file(self, name, f):
        path = self.path / name
        if path.is_file():
            return

        link_or_copy(f, path)
        path.touch()
        self.cleanup()

    @ignore_errors
    def put_bytes(self, name, data):
        path = self.path / name
        if path.is_file():
            return

        path.write_bytes(data)
        self.cleanup()

    def cleanup(self):
        with self.cleanup_lock:
            self.cleanup_counter += 1
            if self.cleanup_counter % self.CLEANUP_FREQ != 0:
                return

        def safe_mtime(f):
            try:
                return f.stat().st_mtime
            except:
                return 0

        files = sorted(
                [f for f in self.path.glob('*') if f.is_file()],
                key=lambda f: safe_mtime(f), reverse=True)

        for f in files[self.max_files:]:
            try:
                os.remove(f)
            except:
                pass

#---------------------------------------------------------------------------
class DoubleCache:
    SIZE_LIMIT = 10 * 1024 * 1024

    def __init__(self, path, max_big_files=128, max_small_files=512, size_limit=SIZE_LIMIT):
        self.size_limit  = size_limit
        self.big_cache   = FileLRUCache(path, max_big_files)
        self.small_cache = FileLRUCache(Path(path) / 'small', max_small_files)

    def get(self, name):
        return self.big_cache.get(name) or self.small_cache.get(name)

    def put_file(self, name, f):
        if f.stat().st_size < self.size_limit:
            self.small_cache.put_file(name, f)
        else:
            self.big_cache.put_file(name, f)

    def put_bytes(self, name, data):
        if len(data) < self.size_limit:
            self.small_cache.put_bytes(name, data)
        else:
            self.big_cache.put_bytes(name, data)

#---------------------------------------------------------------------------
def get_cache():
    try:
        c = Path('.').resolve().parents[1] / 'cache'
        if c.is_dir():
            print(f'Found cache at {c}')
            return DoubleCache(c)
    except:
        print(traceback.format_exc())
        return

    print('Cache not found')

#---------------------------------------------------------------------------
class Server:
    def __init__(self, host):
        self.host    = host
        self.session = make_session()
        self.cache   = get_cache()

    @boxed_request
    def get(self, resource, **kwargs):
        return self.session.get(f'{self.host}{resource}', data=kwargs)

    @boxed_request
    def post(self, resource, files=None, **kwargs):
        return self.session.post(f'{self.host}{resource}', data=kwargs)

    @boxed_request
    def put(self, resource, **kwargs):
        return self.session.put(f'{self.host}{resource}', data=kwargs)

    @boxed_request
    def delete(self, resource, **kwargs):
        return self.session.delete(f'{self.host}{resource}', data=kwargs)

    def get_raw(self, resource, data={}, **kwargs):
        r = self.session.get(f'{self.host}{resource}',
                data={**data, **kwargs},
                **self.request_args)
        r.raise_for_status()
        return r.content

    def object_exists(self, resource):
        return self.get(f'{resource}/exists')

    def get_file(self, object_id, path, executable=False):
        path = Path(path)
        def get_data():
            # If the file is already there, skip the network request
            if path.is_file():
                if file_sha1(path) == object_id:
                    return
                os.remove(path)

            # Check if file is in the cache
            try:
                if self.cache is not None:
                    f = self.cache.get(object_id)
                    if f is not None:
                        link_or_copy(f, path)
                        return
            except:
                pass

            r = self.session.get(f'{self.host}/objects/{object_id}', stream=True)
            r.raise_for_status()

            with open(path, 'wb') as f:
                for chunk in r:
                    f.write(chunk)

            if self.cache is not None:
                self.cache.put_file(object_id, path)

        get_data()

        if executable:
            os.chmod(path, 0o770)

    def stream_object(self, object_id):
        # Check if the object is cached
        try:
            if self.cache is not None:
                f = self.cache.get(object_id)
                if f is not None:
                    return open(f, 'rb')
        except:
            pass

        r = self.session.get(f'{self.host}/objects/{object_id}', stream=True)
        r.raise_for_status()
        r.raw.read = functools.partial(r.raw.read, decode_content=True)
        return r.raw

    def put_file(self, path, object_id=None):
        path = Path(path)

        if object_id is None:
            object_id = file_sha1(path)

        url = self.get(f'/objects/{object_id}/upload')
        if url:
            with open(path, 'rb') as f:
                self.session.put(url, data=f, headers=dict(Authorization=None)).raise_for_status()

        try:
            if self.cache is not None:
                self.cache.put_file(object_id, path)
        except:
            pass

        return object_id

    def put_bytes(self, data):
        object_id = bytes_sha1(data)
        url = self.get(f'/objects/{object_id}/upload')
        if url:
            self.session.put(url, data=data, headers=dict(Authorization=None)).raise_for_status()
        return object_id

    def log_error(self, message):
        try:
            print(message)
            self.post(f'/log', level='error', message=str(message))
        except:
            pass

    def log_info(self, message):
        print(message)
        self.post(f'/log', level='info', message=str(message))
