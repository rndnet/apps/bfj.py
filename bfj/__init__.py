######################################################################################
##
##    Copyright (c) 2021 MR EVGENII BIRIALTCEV, SOLE TRADE <mail@rndnet.net>
##
##    This file is part of RnDnet.
##
##    RnDnet is free software: you can redistribute it and/or modify
##    it under the terms of the GNU General Public License as published by
##    the Free Software Foundation, either version 3 of the License, or
##    (at your option) any later version.
##
##    RnDnet is distributed in the hope that it will be useful,
##    but WITHOUT ANY WARRANTY; without even the implied warranty of
##    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##    GNU General Public License for more details.
##
##    You should have received a copy of the GNU General Public License
##    along with RnDnet.  If not, see <https://www.gnu.org/licenses/>.
#####################################################################################
import h5py
import numpy as np
import io
import json
import matplotlib
import os
import pickle
import shutil
import sys
import zipfile
import time
import traceback

from pathlib           import Path
from binaryornot.check import is_binary
from urllib.request    import urlretrieve

from .server import make_session, get_cache, link_or_copy
from .sha1   import file_sha1

try:
    from .version import __version__
except:
    __version__ = 'dev'

print(f'bfj.py v{__version__}')
print(f'matplotlib v{matplotlib.__version__}')

matplotlib.use('Agg')
package_index = 0

#---------------------------------------------------------------------------
class InputData:
    SIZE_LIMIT = 10 * 1024 * 1024

    def __init__(self):
        self.path    = Path('in').resolve()
        self.tree    = {}
        self.cache   = get_cache()
        self.session = make_session()

        data = self.path / '.data.zip'
        if data.is_file():
            self.z = zipfile.ZipFile(data, 'r')

            for i in self.z.infolist():
                if i.is_dir(): continue

                root = self.tree
                path = Path(i.filename)
                for p in path.parts[:-1]:
                    if p not in root:
                        root[p] = {}
                    root = root[p]
                root[path.parts[-1]] = i.filename

        else:
            print('File "in/.data.zip" not found')

    def packages(self):
        return list(self.tree.keys())

    def package_files(self, package):
        if '.files' in self.tree[package]:
            def get_file(f):
                tic = time.time()

                if self.cache:
                    try:
                        src = self.cache.get(f['hash'])
                        if src is not None:
                            print(f'  {f["name"]}: from cache')
                            return open(src, 'rb')
                    except Exception as e:
                        print(f'  {e}')

                print(f'  {f["name"]}: from server')

                #if f['size'] < self.SIZE_LIMIT:
                #    r = self.session.get(f['link'], stream=True)
                #    r.raise_for_status()
                #    return io.BytesIO(r.content)

                path = self.path / package
                path.mkdir(parents=True, exist_ok=True)

                urlretrieve(f['link'], path / f['name'])

                if self.cache:
                    self.cache.put_file(f['hash'], path / f['name'])

                toc = time.time()
                print(f'  - downloaded {f["name"]} in {toc - tic:.2f}s')

                return open(path / f['name'], 'rb')

            return {Path(f['name']) : get_file(f)
                    for f in json.load(self.z.open(self.tree[package]['.files'], 'r'))}
        else:
            def get_seekable(n):
                f = self.z.open(n, 'r')
                if f.seekable():
                    return f

                p = self.path / n
                p.parent.mkdir(parents=True, exist_ok=True)

                with open(p, 'wb') as t:
                    shutil.copyfileobj(f, t)

                return open(p, 'rb')

            return {Path(k) : get_seekable(v)
                    for k,v in self.tree[package].items()
                    if isinstance(v, str) and k != 'label'}

    def package_save_files(self, package, *suffixes):
        if '.files' in self.tree[package]:
            path = self.path / package
            path.mkdir(parents=True, exist_ok=True)

            def get_file(f):
                tic = time.time()
                dst = path / f["name"]

                if self.cache:
                    try:
                        src = self.cache.get(f['hash'])
                        if src is not None:
                            print(f'  {f["name"]}: from cache')
                            link_or_copy(src, dst)
                            return dst
                    except Exception as e:
                        print(f'  {e}')

                print(f'  {f["name"]}: from server')
                urlretrieve(f['link'], dst)
                toc = time.time()
                print(f'  - downloaded {f["name"]} in {toc - tic:.2f}s')

                if self.cache:
                    self.cache.put_file(f['hash'], dst)

                return dst

            return [get_file(f)
                    for f in json.load(self.z.open(self.tree[package]['.files'], 'r'))
                    if Path(f["name"]).suffix in suffixes]

        return []

    def package_label(self, package):
        p = self.tree[package]
        if 'label' in p and isinstance(p['label'], str):
            return self.z.read(p['label']).decode().strip()
        else:
            return package

    def package_meta(self, package):
        m = {}
        for k,v in self.tree[package].get('meta', {}).items():
            if not isinstance(v, str): continue
            data = self.z.read(v).decode().strip()
            try:
                m[k] = json.loads(data)
            except:
                m[k] = data

        return m

#---------------------------------------------------------------------------
def input_data():
    global _input_data
    if '_input_data' not in globals():
        _input_data = InputData()
    return _input_data

#---------------------------------------------------------------------------
def load_hdf5(f):
    var = dict()

    def load_variable(name, obj):
        if not isinstance(obj, h5py.Dataset): return
        if name.startswith('_'): return

        path = name.split('/')

        if len(path) == 1:
            var[name] = obj[()]
        else:
            head, *path = path

            if head in var:
                g = var[head]
            else:
                g = dict()
                var[head] = g

            while len(path) > 1:
                head, *path = path

                if head in g:
                    c = g[head]
                else:
                    c = dict()
                    g[head] = c
                g = c

            g[path[0]] = obj[()]

    with h5py.File(f, 'r') as hdf:
        hdf.visititems(load_variable)

    return var

#---------------------------------------------------------------------------
def package_meta(p):
    return input_data().package_meta(p)

#---------------------------------------------------------------------------
def load_package(p, scope=None, load_meta=True, ignore_h5_errors=False, clean_up=True):
    path = Path('in', p)
    var  = dict()

    print(f'Loading data for package {p}...')
    tic = time.time()
    for name, stream in input_data().package_files(p).items():
        if name.suffix in ('.h5', '.hdf5'):
            try:
                val = load_hdf5(stream)
                var[name.stem] = next(v for v in val.values()) if len(val) == 1 else val
            except:
                print(f'Skipped "{name}"')
                if not ignore_h5_errors:
                    print(traceback.format_exc())
        else:
            if not (path / name).is_file():
                path.mkdir(parents=True, exist_ok=True)
                with open(path / name, 'wb') as f:
                    for b in iter(lambda: stream.read(65536), b''):
                        f.write(b)

    if load_meta:
        var.update(package_meta(p))

    toc = time.time()
    print(f'done in {toc - tic:.2f}s')

    if scope is None:
        return var

    scope.update(var)

#---------------------------------------------------------------------------
def package_files(p, *suffixes):
    return input_data().package_save_files(p, *suffixes)

#---------------------------------------------------------------------------
def package_label(p):
    return input_data().package_label(p)

#---------------------------------------------------------------------------
def packages():
    return [(p, package_label(p)) for p in input_data().packages()]

#---------------------------------------------------------------------------
def instance_id():
    return int(Path('.').resolve().name)

#---------------------------------------------------------------------------
def read_params(scope, fname=None):
    def read(f):
        params = json.loads(Path(f).read_text())
        scope.update({p['key']: json.loads(p['val']) for p in params})

    if fname is None:
        for f in Path('.').glob('*.prm'):
            if f.is_file():
                read(f)
    else:
        read(fname)

#---------------------------------------------------------------------------
def load_inputs(scope=None, ignore_h5_errors=False):
    var = {}
    read_params(var)

    for p,_ in packages():
        var.update(load_package(p, ignore_h5_errors=ignore_h5_errors))

    if scope is None:
        return var
    else:
        scope.update(var)

#---------------------------------------------------------------------------
def save_file(fname, **kwargs):
    def save_variable(hdf, path, val):
        if isinstance(val, dict):
            for k,v in val.items():
                save_variable(hdf, str(Path(path) / k), v)
        else:
            hdf.create_dataset(path, data=val, track_times=False,
                    compression='gzip' if isinstance(val, np.ndarray) else None)

    with h5py.File(fname, 'w') as f:
        for k,v in kwargs.items():
            save_variable(f, k, v)

#---------------------------------------------------------------------------
def save_package(name=None, label=None, data={}, meta={}, image={}, files={}, save_mpl=False):
    global package_index

    out = Path('out')

    if name is not None:
        path = out / name

        while path.is_dir():
            package_index += 1
            path = out / '{}_{}'.format(name, package_index)

    else:
        package_index += 1
        path = out / str(package_index)

    path.mkdir(parents=True, exist_ok=True)

    if label is not None:
        f = path / 'label'
        f.write_text(str(label))

    for k,v in data.items():
        f = path / f'{k}.h5'
        save_file(f, **{k:v})

    if meta:
        (path / 'meta').mkdir()

    def write_meta(f, val):
        try:
            data = json.dumps(val, ensure_ascii=False)
        except:
            data = str(val)

        f.write_text(data)

    for key,val in meta.items():
        write_meta(path / 'meta' / key, val)

    if image:
        (path / 'image').mkdir()

    for name,fig in image.items():
        f = path / 'image' / name
        if fig.__module__.startswith('matplotlib'):
            if not f.suffix:
                f = f.with_suffix('.png')
            fig.savefig(f, transparent=True)
            if save_mpl:
                pickle.dump(fig, open(f.with_suffix('.mpl'), 'wb'))
        elif fig.__module__.startswith('plotly'):
            if not f.suffix:
                f = f.with_suffix('.html')
            fig.write_html(str(f), include_plotlyjs='cdn')

    for n,w in files.items():
        f = path / n
        f.parent.mkdir(parents=True, exist_ok=True)
        w(open(f, 'wb'))

    return path

#---------------------------------------------------------------------------
def to_string(var):

    if type(var) == str:
        return var

    out_string = ''

    if var.shape[0] > 1 :
        out_string = ''.join(map(chr, var))
    else:
        out_string = var[0]

    return out_string
