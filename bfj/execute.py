######################################################################################
##
##    Copyright (c) 2021 MR EVGENII BIRIALTCEV, SOLE TRADE <mail@rndnet.net>
##
##    This file is part of RnDnet.
##
##    RnDnet is free software: you can redistribute it and/or modify
##    it under the terms of the GNU General Public License as published by
##    the Free Software Foundation, either version 3 of the License, or
##    (at your option) any later version.
##
##    RnDnet is distributed in the hope that it will be useful,
##    but WITHOUT ANY WARRANTY; without even the implied warranty of
##    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##    GNU General Public License for more details.
##
##    You should have received a copy of the GNU General Public License
##    along with RnDnet.  If not, see <https://www.gnu.org/licenses/>.
#####################################################################################
import io
import json
import os
import pickle
import re
import requests
import subprocess
import sys
import time
import traceback
import zipfile
import threading

from collections       import defaultdict
from itertools         import chain
from binaryornot.check import is_binary
from contextlib        import contextmanager
from datetime          import datetime, timedelta
from pathlib           import Path, PosixPath
from shutil            import rmtree

from .server import Server
from .sha1   import file_sha1

#---------------------------------------------------------------------------
@contextmanager
def timing(server, instance, message):
    try:
        tic = datetime.now()
        yield
    finally:
        server.log_info(f'Instance {instance}: {message} in {datetime.now() - tic}')

#---------------------------------------------------------------------------
def download_package(server, instance, archive, node, package):
    if isinstance(package, int):
        package = server.get(f'/packages/{package}')

    path    = Path(str(package.id))
    inputs  = [re.compile(k) for k in node.specification.input]
    mapping = json.loads(package.mapping) if package.mapping else []
    var_map = {f:t for f,t in mapping}

    def write_text(path, data):
        with archive.open(str(path), 'w', force_zip64=True) as dst:
            dst.write(data.encode())

    def write_file(path, f):
        stream = server.stream_object(f.hash)
        with archive.open(str(path), 'w', force_zip64=True) as dst:
            for chunk in iter(lambda: stream.read(32768), b''):
                dst.write(chunk)

    if package.label: write_text(path / 'label', package.label)

    if all('link' in f for f in package.files):
        files = []
        for f in package.files:
            fpath = Path(f.name)
            fpath = fpath.with_name('{}{}'.format(var_map.get(fpath.stem, fpath.stem), fpath.suffix))
            if len(inputs) == 0 or any(i.fullmatch(fpath.stem) for i in inputs):
                f.name = fpath.name
                files.append(f)
            else:
                print(f'Skipped variable {f.name} in package {package.id}: not in specification')
        write_text(path / '.files', json.dumps(files, indent=2, ensure_ascii=False))
    else:
        for f in package.files:
            fpath = Path(f.name)
            fpath = fpath.with_name('{}{}'.format(var_map.get(fpath.stem, fpath.stem), fpath.suffix))
            if len(inputs) == 0 or any(i.fullmatch(fpath.stem) for i in inputs):
                write_file(path / fpath, f)
            else:
                print(f'Skipped variable {f.name} in package {package.id}: not in specification')

    meta_path  = path / 'meta'
    for key, val in package.meta.items():
        fpath = meta_path / var_map.get(key, key)
        if len(inputs) == 0 or any(i.fullmatch(fpath.name) for i in inputs):
            write_text(fpath, val)

    for p in package.get('packages', []):
        with timing(server, instance, f'downloaded nested package {p}'):
            download_package(server, instance, archive, node, p)

#---------------------------------------------------------------------------
def download_instance(server, instance_id, keep_files):
    i = server.get(f'/instances/{instance_id}/for_scheduler')

    private_link = os.environ.get('RNDNET_PRIVATE_DATA', None)
    private = False
    if private_link is not None:
        private = True
        r = requests.get(private_link, verify=False)
        r.raise_for_status()
        data = pickle.loads(r.content)
        for n in data['node']:
            if n.label == i.node.label:
                for f in n.files:
                    with open(f.name, 'wb') as t:
                        t.write(data['blob'][f.hash])
                    if f.executable:
                        os.chmod(f.name, 0o770)
                Path('.exclude').write_text('\n'.join(
                    [f.name for f in n.files] + ['.exclude']))
                i.node.compute.script = n.compute.script
                break

    if not keep_files:
        download_files = {}

        for f in i.node.files:
            download_files[Path(f.name)] = (f.hash, f.executable)

        in_dir = Path('in')
        in_dir.mkdir(exist_ok=True)

        Path('out').mkdir(exist_ok=True)

        for f in i.files:
            if f.name.startswith('out'):
                continue
            filepath = Path(f.name)
            filepath.parent.mkdir(parents=True, exist_ok=True)
            download_files[filepath] = (f.hash, f.executable)

        for p,(h,e) in download_files.items():
            server.get_file(h, p, e)

        Path('.prm').write_text(json.dumps(i.params, ensure_ascii=False, indent=2))

        with zipfile.ZipFile(in_dir / '.data.zip', 'w', compression=zipfile.ZIP_DEFLATED) as z:
            for p in i.in_packages + i.pull_packages:
                with timing(server, instance_id, f'downloaded package {p.id}'):
                    download_package(server, instance_id, z, i.node, p)

    return i

#---------------------------------------------------------------------------
def upload_instance(server, instance, start, ret_code):
    pwd = Path('.').resolve()

    for f in pwd.glob('*.prm'):
        params = f.read_text()
        break
    else:
        params = None

    exclude = pwd / '.exclude'
    if exclude.is_file():
        exclude_files = set(pwd / f.strip() for f in open(exclude))
    else:
        exclude_files = set()

    def collect_file(f):
        return dict(
                name       = str(f.relative_to(pwd)),
                hash       = file_sha1(f),
                executable = os.access(f, os.X_OK),
                is_binary  = is_binary(str(f)),
                size       = f.stat().st_size)

    def collect_package(path):
        if not path.is_dir():
            return []

        for root, dirs, flist in os.walk(path):
            for f in flist:
                yield Path(root) / f

    with timing(server, instance.id, 'gathered files'):
        files = [collect_file(f) for f in pwd.glob('*')
                 if f.is_file() and f not in exclude_files]

        if ret_code == 0:
            files += [collect_file(f)
                      for p in (pwd / 'out').glob('*')
                      for f in collect_package(p)]

        unique_files = defaultdict(list)
        for f in files:
            unique_files[f['hash']].append(f)

        hashes = list(unique_files)

    chunk = 1000
    for i in range(0, len(hashes), chunk):
        with timing(server, instance.id, f'uploaded files ({min(len(hashes),i+chunk)} / {len(hashes)})'):
            upload = server.get(f'/objects/upload', object_ids=hashes[i:i+chunk])
            for h,l in upload.items():
                with open(pwd / unique_files[h][0]['name'], 'rb') as f:
                    server.session.put(l, data=f, headers=dict(Authorization=None)).raise_for_status()

    if server.get(f'/instances/{instance.id}/exists'):
        server.put(f'/instances/{instance.id}',
                ret_code=ret_code,
                runtime=(datetime.now() - start).total_seconds(),
                params=params,
                files=json.dumps(files))
    else:
        server.log_info(f'Instance {instance.id} has been removed from server')

#---------------------------------------------------------------------------
def heartbeat(server, instance_id, done):
    interval = timedelta(seconds=60)
    chkpt = None
    while True:
        time.sleep(0.1)
        if done.is_set(): break
        if chkpt is None or datetime.now() >= chkpt:
            try:
                server.put(f'/instances/{instance_id}/heartbeat')
            except:
                pass
            chkpt = datetime.now() + interval

#---------------------------------------------------------------------------
def execute(server, instance_id, keep_files, start):
    try:
        done = threading.Event()
        t = threading.Thread(target=heartbeat, args=(server, instance_id, done))
        t.start()

        with timing(server, instance_id, 'downloaded'):
            instance = download_instance(server, instance_id, keep_files)

        for d in Path('out').glob('*'):
            rmtree(d)

        with timing(server, instance_id, 'executed user script'):
            env = os.environ.copy()
            env.pop('RNDNET_TOKEN', None)
            env.pop('RNDNET_PRIVATE_DATA', None)

            fallback = """
echo "Empty script: nothing to do :("
exit 1
"""
            print(f'Starting user script:\n{instance.node.compute.script}')
            tic = datetime.now()
            with subprocess.Popen(f"""(
{instance.node.compute.script or fallback}
) 2>&1 | tee {instance_id}.log
rc=${{PIPESTATUS[0]}}
exit $rc
""", shell=True, executable="/bin/bash", stdout=subprocess.PIPE) as p:
                log = b''
                for line in iter(p.stdout.readline, b''):
                    log += line
                    try:
                        print(line.decode().rstrip())
                    except:
                        pass

                    if (datetime.now() - tic).total_seconds() < 60:
                        continue

                    tic = datetime.now()
                    if instance.interactive:
                        server.put(f'/instances/{instance_id}/files',
                            files=json.dumps([dict(
                                name=f'{instance_id}.log',
                                hash=server.put_bytes(log),
                                size=len(log),
                                executable=False,
                                is_binary=False)]))

        os.sync()

        with timing(server, instance_id, f'uploaded results, ret_code={p.returncode}'):
            upload_instance(server, instance, start, p.returncode)

        return p.returncode

    finally:
        done.set()
        t.join()

#---------------------------------------------------------------------------
def main():
    import argparse
    from getpass import getpass
    from urllib.parse import urlparse, urlunparse

    parser = argparse.ArgumentParser()
    parser.add_argument('-r,--root',     dest='root',     required=True)
    parser.add_argument('-i,--instance', dest='instance', required=True, type=int)
    parser.add_argument('-k,--keep',     dest='keep',     action='store_true')
    args = parser.parse_args()

    if 'RNDNET_TOKEN' not in os.environ:
        if sys.stdout.isatty():
            user   = input('login: ')
            passwd = getpass('password: ')
            u = urlparse(args.root)
            t = f'{urlunparse((u.scheme, u.netloc, str(PosixPath(u.path).parents[1]), None, None, None))}/token/{user}'
            r = requests.post(t, data=dict(passwd=passwd))
            r.raise_for_status()
            os.environ['RNDNET_TOKEN'] = r.json()
        else:
            raise Exception('Access token not found')

    start  = datetime.now()
    server = Server(args.root)
    try:
        with timing(server, args.instance, 'done'):
            rc = execute(server, args.instance, args.keep, start)
    except:
        err = f"""Error in instance {args.instance}:\n{traceback.format_exc()}"""
        server.log_error(err)
        rc = 101
        f = Path(f'{args.instance}.log')
        log = f'{f.read_text()}\n{err}' if f.is_file() else f'{err}'
        if server.get(f'/instances/{args.instance}/exists'):
            server.put(f'/instances/{args.instance}',
                    ret_code=rc,
                    runtime=(datetime.now() - start).total_seconds(),
                    files=json.dumps([dict(
                        name       = f'{args.instance}.log',
                        hash       = server.put_bytes(log.encode()),
                        executable = False,
                        is_binary  = False,
                        size       = len(log.encode()))
                        ]))

#---------------------------------------------------------------------------
if __name__ == "__main__":
    main()
