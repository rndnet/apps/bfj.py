######################################################################################
##
##    Copyright (c) 2021 MR EVGENII BIRIALTCEV, SOLE TRADE <mail@rndnet.net>
##
##    This file is part of RnDnet.
##
##    RnDnet is free software: you can redistribute it and/or modify
##    it under the terms of the GNU General Public License as published by
##    the Free Software Foundation, either version 3 of the License, or
##    (at your option) any later version.
##
##    RnDnet is distributed in the hope that it will be useful,
##    but WITHOUT ANY WARRANTY; without even the implied warranty of
##    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##    GNU General Public License for more details.
##
##    You should have received a copy of the GNU General Public License
##    along with RnDnet.  If not, see <https://www.gnu.org/licenses/>.
#####################################################################################
import hashlib

#---------------------------------------------------------------------------
def file_sha1(path):
    chunk = 65536

    h = hashlib.sha1()
    with open(path, 'rb') as f:
        for b in iter(lambda: f.read(chunk), b''):
            h.update(b)

    return h.hexdigest()

#---------------------------------------------------------------------------
def bytes_sha1(array):
    chunk = 65536

    h = hashlib.sha1()
    for b in (array[i:i+chunk] for i in range(0, len(array), chunk)):
        h.update(b)

    return h.hexdigest()
