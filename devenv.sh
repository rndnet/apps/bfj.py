#!/bin/bash
dir=$(cd $(dirname "${BASH_SOURCE}") > /dev/null; pwd -P)
export PATH=${dir}/bin:${PATH}
export PYTHONPATH=${dir}:${PYTHONPATH}
