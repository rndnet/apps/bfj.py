######################################################################################
##
##    Copyright (c) 2021 MR EVGENII BIRIALTCEV, SOLE TRADE <mail@rndnet.net>
##
##    This file is part of RnDnet.
##
##    RnDnet is free software: you can redistribute it and/or modify
##    it under the terms of the GNU General Public License as published by
##    the Free Software Foundation, either version 3 of the License, or
##    (at your option) any later version.
##
##    RnDnet is distributed in the hope that it will be useful,
##    but WITHOUT ANY WARRANTY; without even the implied warranty of
##    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##    GNU General Public License for more details.
##
##    You should have received a copy of the GNU General Public License
##    along with RnDnet.  If not, see <https://www.gnu.org/licenses/>.
#####################################################################################
from setuptools import setup, find_packages
from git_version import git_version

setup(
        name='bfj',
        author='MR EVGENII BIRIALTCEV, SOLE TRADE',
        author_email='mail@rndnet.net',
        version=git_version(),
        description='Support module for python job scripts for Rndnet.net cloud',
        include_package_data=True,
        packages=find_packages(),
        entry_points={
            'console_scripts': [
                'bfj_execute=bfj.execute:main'
                ]
            },
        )
